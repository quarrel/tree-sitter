#!/bin/bash
trap 'rm -f "$TMPFILE"' EXIT
CACHE_REPL_DIR="$HOME/.cache/quarrel/repl"
mkdir -p $CACHE_REPL_DIR
TMPFILE=$(mktemp -p $CACHE_REPL_DIR)||exit 1
echo "Using $TMPFILE for staging..."

SPINNER=""
echo "Using Fira Code spinner..."

while true; do
  read input
  
  echo -n ' '
  while true; do
    sleep 0.05
    printf "\r${SPINNER:i++%${#SPINNER}:1}"
  done &
  SPINNER_PID=$!
  ./node_modules/tree-sitter-cli/tree-sitter parse <(echo "$input") > $TMPFILE
  kill "$SPINNER_PID"
  printf "\r"
  cat $TMPFILE
done
