#include "tree_sitter/parser.h"
#include <cstdint>
#include <vector>
#include <cwctype>
#include <cstring>
#include <cassert>
#include <iostream>

#ifdef TREE_SITTER_INTERNAL_BUILD
  #define FAIL                                           \
    do {                                                 \
      std::cout << "fail on: " << __LINE__ << std::endl; \
      abort();                                           \
    } while (0)
#else
  #define FAIL \
    return false
#endif

#define SUCCEED \
  return true

namespace {

enum TokenType {
  NEWLINE,
  INDENT,
  DEDENT,
  STRING_START,
  STRING_CONTENT,
  STRING_END
};

struct Delimiter {
  enum {
    SingleQuote = 1 << 0,
    DoubleQuote = 1 << 1,
    TripleQuote = 1 << 2,
    InterpoKind = 1 << 3,
    RawKind = 1 << 4
  };
  char flags;
  
  Delimiter() : flags(0) {}
  
  bool is_triple() const { return flags & TripleQuote; }
  void set_triple() { flags |= TripleQuote; }
  
  bool is_interpo() const { return flags & InterpoKind; }
  void set_interpo() { flags |= InterpoKind; }
  
  bool is_raw() const { return flags & RawKind; }
  void set_raw() { flags |= RawKind; }
  
  int32_t end_character() const {
    return flags & SingleQuote ? '\''
         : flags & DoubleQuote ? '"'
         : 0; 
  }
  void set_end_character(int32_t character) {
    switch (character) {
      case '\'': flags |= SingleQuote; break;
      case '"' : flags |= DoubleQuote; break;
      default: assert(false); 
    }
  }
};

struct Scanner {
  
  std::vector<uint16_t> indent_length_stack;
  std::vector<Delimiter> delimiter_stack;

#ifdef TREE_SITTER_INTERNAL_BUILD
  bool debug;
#endif
  
#ifdef TREE_SITTER_INTERNAL_BUILD
  Scanner(bool debug) {
    this->debug = debug;
#else
  Scanner() {
#endif
    assert(sizeof(Delimiter) == sizeof(char));
    deserialize(NULL, 0);
  }
  
  unsigned serialize(char* buffer) {
    size_t i = 0;
    
    size_t stack_size = delimiter_stack.size();
    if (stack_size > UINT8_MAX) stack_size = UINT8_MAX;
    buffer[i++] = stack_size;
    
    memcpy(&buffer[i], delimiter_stack.data(), stack_size);
    i += stack_size;
    
    std::vector<uint16_t>::iterator
      iter = indent_length_stack.begin() + 1,
      end = indent_length_stack.end();

    for (; iter != end && i < TREE_SITTER_SERIALIZATION_BUFFER_SIZE; ++iter) {
      buffer[i++] = *iter;
    }
    return i;
  }

  void deserialize(const char* buffer, unsigned length) {
    delimiter_stack.clear();
    indent_length_stack.clear();
    indent_length_stack.push_back(0);
    if (length > 0) {
      size_t i = 0;
      
      size_t delimiter_count = (uint8_t)buffer[i++];
      delimiter_stack.resize(delimiter_count);
      memcpy(delimiter_stack.data(), &buffer[i], delimiter_count);
      i += delimiter_count;
      
      for (; i < length; i++) indent_length_stack.push_back(buffer[i]);
    }
  }
  
  bool scan(TSLexer* lexer, const bool* valid_symbols) {
    if (valid_symbols[STRING_CONTENT] && !valid_symbols[INDENT] && !delimiter_stack.empty()) {
      Delimiter delimiter = delimiter_stack.back();
      int32_t end_character = delimiter.end_character();
      bool has_content = false;
      while (lexer->lookahead) {
        if (lexer->lookahead == '~' && delimiter.is_interpo()) {
          lexer->mark_end(lexer);
          lexer->advance(lexer, false);
          if (lexer->lookahead == '{' || std::iswalpha(lexer->lookahead)) {
            lexer->result_symbol = STRING_CONTENT;
            return has_content;
          }
        } else if (lexer->lookahead == '\\') {
          if (delimiter.is_raw()) {
            lexer->advance(lexer, false);
          } else {
            lexer->mark_end(lexer);
            lexer->result_symbol = STRING_CONTENT;
            return has_content;
          }
        } else if (lexer->lookahead == end_character) {
          if (delimiter.is_triple()) {
            lexer->mark_end(lexer);
            lexer->advance(lexer, false);
            if (lexer->lookahead == end_character) {
              lexer->advance(lexer, false);
              if (lexer->lookahead == end_character) {
                if (has_content) {
                  lexer->result_symbol = STRING_CONTENT;
                } else {
                  lexer->advance(lexer, false);
                  lexer->mark_end(lexer);
                  delimiter_stack.pop_back();
                  lexer->result_symbol = STRING_END;
                }
                SUCCEED;
              }
            }
          } else {
            if (has_content) {
              lexer->result_symbol = STRING_CONTENT;
            } else {
              lexer->advance(lexer, false);
              delimiter_stack.pop_back();
              lexer->result_symbol = STRING_END;
            }
            lexer->mark_end(lexer);
            SUCCEED;
          }
        } else if (lexer->lookahead == '\n' && has_content && !delimiter.is_triple()) {
          peek(lexer);
          FAIL;
        }
        // prevent eating of interpolation variables
        if (lexer->lookahead != '~') lexer->advance(lexer, false);
        has_content = true;
      } // while (lexer->lookahead)
    } // if (valid_symbols[STRING_CONTENT] && !valid_symbols[INDENT] && !delimiter_stack.empty())    

    lexer->mark_end(lexer); // don't capture any tokens
    
    bool found_end_of_line = false;
    uint32_t indent_length = 0;
    int32_t first_comment_indent_length = -1;
    
    for (;;) {
      if (lexer->lookahead == '\n') {
        found_end_of_line = true;
        indent_length = 0;
        lexer->advance(lexer, true);
      } else if (lexer->lookahead == ' ') {
        indent_length++;
        lexer->advance(lexer, true);
      } else if (lexer->lookahead == '\r') {
        indent_length = 0;
        lexer->advance(lexer, true);
      } else if (lexer->lookahead == '\t') {
        indent_length += 8;
        lexer->advance(lexer, true);
      } else if (lexer->lookahead == '#') {
        if (first_comment_indent_length == -1)
          first_comment_indent_length = (int32_t)indent_length;
        while (lexer->lookahead && lexer->lookahead != '\n') lexer->advance(lexer, true);
        lexer->advance(lexer, true);
        indent_length = 0;
      } else if (lexer->lookahead == '\\') {
        lexer->advance(lexer, true);
        if (lexer->lookahead == '\r') lexer->advance(lexer, true);
        if (lexer->lookahead == '\n') lexer->advance(lexer, true);
        else FAIL;
      } else if (lexer->lookahead == '\f') {
        indent_length = 0;
        lexer->advance(lexer, true);
      } else if (lexer->lookahead == 0) {
        indent_length = 0;
        found_end_of_line = true;
        break;
      } else { break; }
    }
      
    if (found_end_of_line) {
      if (!indent_length_stack.empty()) {
        uint16_t current_indent_length = indent_length_stack.back();
        
        if (
          valid_symbols[INDENT] &&
          indent_length > current_indent_length
        ) {
          indent_length_stack.push_back(indent_length);
          announce(lexer, "indent");
          peek(lexer);
          lexer->result_symbol = INDENT;
          SUCCEED;
        }
        
        if (
          valid_symbols[DEDENT] &&
          indent_length < current_indent_length &&
          
          // wait to create a dedent token until we've consumed any comments
          // whose indentation matches the current block.
          first_comment_indent_length < (int32_t)current_indent_length
        ) {
          indent_length_stack.pop_back();
          announce(lexer, "dedent");
          peek(lexer);
          lexer->result_symbol = DEDENT;
          SUCCEED;
        }
        
      }
        
      if (
        valid_symbols[NEWLINE]
      ) {
        announce(lexer, "newline");
        peek(lexer);
        lexer->result_symbol = NEWLINE;
        SUCCEED;
      }
      
    }
          
    if (first_comment_indent_length == -1 && valid_symbols[STRING_START]) {
      Delimiter delimiter;
      
      if (lexer->lookahead == '\'') {
        delimiter.set_raw();
        delimiter.set_end_character('\'');
        lexer->advance(lexer, false);
        lexer->mark_end(lexer);
        if (lexer->lookahead == '\'') {
          lexer->advance(lexer, false);
          if (lexer->lookahead == '\'') {
            delimiter.set_triple();
            lexer->advance(lexer, false);
            lexer->mark_end(lexer);
          }
        }
      } else if (lexer->lookahead == '"') {
        delimiter.set_interpo();
        delimiter.set_end_character('"');
        lexer->advance(lexer, false);
        lexer->mark_end(lexer);
        if (lexer->lookahead == '"') {
          lexer->advance(lexer, false);
          if (lexer->lookahead == '"') {
            delimiter.set_triple();
            lexer->advance(lexer, false);
            lexer->mark_end(lexer);
          }
        } 
      }
      
      if (delimiter.end_character()) {
        delimiter_stack.push_back(delimiter);
        lexer->result_symbol = STRING_START;
        SUCCEED;
      }
    } // if (first_comment_indent_length == -1 && vali_symbols[STRING_START])
      
    return false;
  } // bool scan(TSLexer *lexer, const bool *valid_symbols)
  
  void peek(TSLexer* lexer) {
  #ifdef TREE_SITTER_INTERNAL_BUILD
    if (debug) {
      std::string s;
      switch (lexer->lookahead) {
        case 0x20: s = "\\s"; break;
        case '\t': s = "\\t"; break;
        case '\n': s = "\\n"; break;
        case '\v': s = "\\v"; break;
        case '\f': s = "\\f"; break;
        case '\r': s = "\\r"; break;
        default: s = lexer->lookahead; break;
      }
      std::cout << "  lookahead: " << s << ", char: " << lexer->lookahead << std::endl;
    }
  #endif
  } 
 
  void announce(TSLexer* lexer, const std::string token) {
  #ifdef TREE_SITTER_INTERNAL_BUILD
    if (debug) {
      std::cout << "  returning " << token << " token at col: " << lexer->get_column(lexer) << std::endl;
    }    
  #endif
  }
}; // struct Scanner

} // anon namespace

extern "C" {

void* tree_sitter_quarrel_external_scanner_create() {
  #ifdef TREE_SITTER_INTERNAL_BUILD
    return new Scanner(std::getenv("TREE_SITTER_DEBUG"));
  #else
    return new Scanner();
  #endif
}

void tree_sitter_quarrel_external_scanner_destroy(void *payload) {
  Scanner* scanner = static_cast<Scanner*>(payload);
  delete scanner;
}

unsigned tree_sitter_quarrel_external_scanner_serialize(
  void *payload,
  char *buffer
) {
  Scanner* scanner = static_cast<Scanner*>(payload);
  return scanner->serialize(buffer);
}

void tree_sitter_quarrel_external_scanner_deserialize(
  void *payload,
  const char *buffer,
  unsigned length
) {
  Scanner* scanner = static_cast<Scanner*>(payload);
  scanner->deserialize(buffer, length);
}

bool tree_sitter_quarrel_external_scanner_scan(
  void *payload,
  TSLexer *lexer,
  const bool *valid_symbols
) {
  Scanner* scanner = static_cast<Scanner*>(payload);
  return scanner->scan(lexer, valid_symbols);
}
  
} // extern "C"
