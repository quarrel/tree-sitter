#!/bin/bash
SPINNER=""
echo "Using Fira Code spinner..."

FILE=grammar.js
while watchman-wait $FILE; do
    clear
    echo -n ' '
    while true; do
    sleep 0.05
    printf "\r${SPINNER:i++%${#SPINNER}:1}"
    done &
    SPINNER_PID=$!
        
    ./node_modules/.bin/tree-sitter generate
    GEN_RESULT=$?
    kill "$SPINNER_PID"
    printf "\r"
    if [ $GEN_RESULT = '0' ]; then
        echo 'Parser generated successfully!'
    fi
done
