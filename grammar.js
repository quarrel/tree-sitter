const PREC = {
  conditional: -1,
  parenthesized_expression: 1,
  parenthesized_array_spread: 1,
  negative: 1,
  comparative: 2,
  objective: 3,
  disjunctive: 10,
  conjunctive: 11,
  additive: 16,
  multiplicative: 17,
  unary: 18,
  exponential: 19,
  invocative: 50,
}

module.exports = grammar({
  name: 'quarrel',
  externals: $ => [
    $.newline,
    $.indent,
    $.dedent,
    $._string_start,
    $._string_fragment,
    $._string_end
  ],
  conflicts: $ => [
    [$.primary_expression, $.pattern],
    [$.primary_expression, $.array_spread_pattern],
    [$.primary_expression, $.pattern, $.parameter],
    [$.pattern, $.parameter],
    [$.pattern, $._collection_elements],
    [$.parameter, $.primary_expression],
    [$.tuple, $.tuple_pattern],
    [$.array, $.array_pattern],
    [$._expressions, $.expression_list],
    [$.expression_statement, $._expression_within_for_in_clause],
    [$.argument_list, $.object],
    // [$.value_equal_operator, $.value_equal_not_operator, $.value_match_not_operator, $.identity_equal_not_operator, $.instance_equal_not_operator],
    [$.cast_expression, $.slice],
  ],
  supertypes: $ => [
    $._simple_statement,
    $._compound_statement,
    $.expression,
    $.primary_expression,
    $.pattern,
    $.parameter
  ],
  extras: $ => [
    $.comment,
    /[\s\uFEFF\u2060\u200B]|\\\r?\n/
  ],
  word: $ => $.identifier,
  rules: {
    root: $ => seq(
      optional($.hash_bang_line),
      repeat($.line),
    ),
    
    hash_bang_line: () => /#!.*/,
    
    line: $ => seq(
      sep1($._statement, ';'),
      optional(';'),
      $.newline
    ),
    _statement: $ => choice(
      $._simple_statements,
      $._compound_statement
    ),
    _simple_statements: $ => seq(
      sep1($._simple_statement, $.semicolon),
      optional($.semicolon),
      $.newline
    ),
    _simple_statement: $ => choice(
      $.break_statement,
      $.continue_statement,
      $.expression_statement,
      $.return_statement,
    ),
    expression_statement: $ => choice(
      $.expression,
      seq(sep1($.expression, $.comma), optional($.comma)),
      $.assignment_expression,
      $.augmented_assignment_expression,
      $.yield_expression,  
    ),
    return_statement: $ => seq(
      'return',
      optional($._expressions)
    ),
    delete_statement: $ => seq(
      'delete',
      $._expressions  
    ),
    break_statement: $ => seq(  
      'break',
      optional($._expressions)
    ),
    continue_statement: $ => seq(
      'continue',
      optional($._expressions)
    ),
    _expressions: $ => choice(
      $.expression,
      $.expression_list  
    ),
    _compound_statement: $ => choice(
      $.if_statement,
      $.for_statement,
      $.while_statement,
      $.try_statement,
      $.given_statement
    ),
    if_statement: $ => seq(
      'if',
      field('condition', $.expression),
      'then',
      field('consequence', $._suite),
      repeat(field('alternative', $.elif_clause)),
      optional(field('alternative', $.else_clause))
    ),
    elif_clause: $ => seq(
      'elif',
      field('condition', $.expression),
      'then',
      field('consequence', $._suite)
    ),
    else_clause: $ => seq(
      'else',
      field('body', $._suite)
    ),
    given_statement: $ => seq(
      'given',
      sep1(field('subject', $.expression), $.comma),
      optional($.comma),
      'then',
      repeat(field('alternative', $.when_clause))
    ),
    when_clause: $ => seq(
      'when',
      sep1(field('pattern',
        alias(choice($.expression, $.array_spread_pattern), $.when_pattern)
      ), $.comma),
      optional($.comma),
      optional(field('guard', $.if_clause)),
      'then',
      field('consequence', $._suite)
    ),
    for_statement: $ => seq(
      optional('async'),
      'for',
      field('left', $._left_hand_side),
      'in',
      field('right', $._expressions),
      'then',
      field('body', $._suite),
      optional(field('alternative', $.else_clause))
    ),
    while_statement: $ => seq(
      'while',
      field('condition', $.expression),
      'then',
      field('body', $._suite),
      optional(field('alternative', $.else_clause)),
    ),
    try_statement: $ => seq(
      'try',
      field('body', $._suite),
      choice(
        seq(repeat1($.catch_clause), optional($.else_clause), optional($.finally_clause)),
        $.finally_clause
      )
    ),
    catch_clause: $ => seq(
      'catch',
      optional(seq($.expression, optional(seq(choice('as', $.comma), $.expression)))),
      'then',
      $._suite
    ),
    finally_clause: $ => seq(
      'finally',
      $._suite
    ),
    function_expression: $ => seq(
      field('parameters', $.parameters),
      $.function_operator,
      field('body', $._suite)
    ),
    parameters: $ => seq('(', optional($._parameters), ')'),
    function_operator: () => /\|?\|?[\-=]>/,
    array_spread: $ => seq('..', $.expression),
    object_spread: $ => seq('...', $.expression),
    parenthesized_array_spread: $ => prec(PREC.parenthesized_array_spread, seq(
      '(',
      choice(
        alias($.parenthesized_array_spread, $.parenthesized_expression),
        $.array_spread
      ),
      ')'
    )),
    argument_list: $ => seq(
      token.immediate('('),
      optional(sep1(
        choice(
          $.expression,
          $.array_spread,
          $.object_spread,
          alias($.parenthesized_array_spread, $.parenthesized_expression),
          $.keyword_argument
        ),
        $.comma
      )),
      optional($.comma),
      ')'
    ),
    _suite: $ => choice(
      alias($._simple_statements, $.block),
      seq($.indent, $.block),
      alias($.newline, $.block)
    ),
    block: $ => seq(
      repeat($._statement),
      $.dedent
    ),
    expression_list: $ => prec.right(seq(
      $.expression,
      choice(
        $.comma,
        seq(repeat1(seq($.comma, $.expression)), optional($.comma))
      )
    )),
    slashed_name: $ => sep1($.identifier, '\\'),
    
    // Patterns
    
    _parameters: $ => seq(
      sep1($.parameter, $.comma),
      optional($.comma)
    ),
    
    _patterns: $ => seq(
      sep1($.pattern, $.comma),
      optional($.comma)
    ),
    
    parameter: $ => choice(
      $.identifier,
      $.default_parameter,
      $.array_spread_pattern,
      $.tuple_pattern,
      $.object_spread_pattern
    ),
    
    pattern: $ => choice(
      $.identifier,
      $.keyword_identifier,
      $.subscript,
      $.attribute,
      $.array_spread_pattern,
      $.tuple_pattern,
      $.array_spread
    ),
    
    tuple_pattern: $ => seq('{', optional($._patterns), '}'),
    
    array_pattern: $ => seq('[', optional($._patterns), ']'),
    
    default_parameter: $ => seq(
      field('name', $.identifier),
      field('operator', choice(
        ':=',
        '.='
      )),
      field('value', $.expression),
    ),
    
    array_spread_pattern: $ => seq(
      '..', choice($.identifier, $.keyword_identifier, $.subscript, $.attribute)
    ),
    
    object_spread_pattern: $ => prec(PREC.unary, seq(
      '...', choice($.identifier, $.keyword_identifier, $.subscript, $.attribute)
    )),
    
    // Expressions
    

    _expression_within_for_in_clause: $ => choice(
      $.expression,
      alias($.function_within_for_in_clause, $.function_expression)
    ),
    
    expression: $ => choice(
      $.comparison_expression,
      $.not_operator,
      $.boolean_operator,
      $.await,
      $.function_expression,
      $.primary_expression,
      $.conditional_expression,
    ),
    
    primary_expression: $ => choice(
      $.binary_expression,
      $.cast_expression,
      $.identifier,
      $.keyword_identifier,
      $.symbol,
      $.string,
      $.integer,
      $.float,
      $.type,
      $.true,
      $.false,
      $.none,
      $.empty,
      $.unary_expression,
      $.attribute,
      $.subscript,
      $.call,
      $.array,
      $.array_comprehension,
      $.object,
      $.object_comprehension,
      $.set,
      $.set_comprehension,
      $.lexicon,
      $.tuple,
      $.parenthesized_expression,
      $.generator_expression,
    ),
    not_operator: $ => prec(PREC.negative, seq(
      choice('not', 'no'),
      field('argument', $.expression)
    )),
    
    boolean_operator: $ => choice(
      prec.left(PREC.conjunctive, seq(
        field('left', $.expression),
        field('operator', 'and'),
        field('right', $.expression)
      )),
      prec.left(PREC.disjunctive, seq(
        field('left', $.expression),
        field('operator', 'or'),
        field('right', $.expression)
      ))
    ),
    
    binary_expression: $ => {
      const table = [
        [prec.left, '++', PREC.additive],
        [prec.left, '--', PREC.additive],
        [prec.left, '**', PREC.multiplicative],
        [prec.left, '&&', PREC.multiplicative],
        [prec.left, '//', PREC.multiplicative],
        [prec.left, '%%', PREC.multiplicative],
        [prec.left, '+/', PREC.multiplicative],
        [prec.left, '-/', PREC.multiplicative],
        [prec.right, '^^', PREC.exponential],
      ];
      return choice(...table.map(([fn, operator, precedence]) => fn(precedence, seq(
        field('left', $.primary_expression),
        field('operator', operator),
        field('right', $.primary_expression)
      ))));
    },
    
    unary_expression: $ => prec(PREC.unary, seq(
      field('operator', choice('+', '-', '~', '*', '%', '^', '&', '@')),
      field('argument', $.primary_expression) 
    )),
    
    comparison_expression: $ => prec.left(PREC.parenthesized_expression, seq(
      $.primary_expression,
      repeat1(prec.right(PREC.comparative, seq(
        field('operator', choice(
          alias('<<', $.less_than_operator),
          alias('<=', $.less_than_or_equal_operator),
          alias('>>', $.greater_than_operator),
          alias('>=', $.greater_than_or_equal_operator),
          alias('%?', $.divisible_by_operator),
          alias(token(choice('==', 'is')), $.value_equal_operator),
          alias(token(choice('!=', seq('is', / +/, 'not'), 'isnt')), $.value_equal_not_operator),
          alias(token(choice('=~', seq('is', / +/, 'like'))), $.value_match_operator),
          alias(token(choice('!~', seq('is', / +/, 'not', / +/, 'like'), seq('isnt', / +/, 'like'))), $.value_match_not_operator),
          alias(token(choice('===', seq('is', / +/, 'the'))), $.identity_equal_operator),
          alias(token(choice('!==' , seq('is', / +/, 'not', / +/, 'the'), seq('isnt', / +/, 'the'))), $.identity_equal_not_operator),
          alias(token(choice('=:=', seq('is', / +/, choice('a', 'an')))), $.instance_equal_operator),
          alias(token(choice('=!=', seq('is', / +/, 'not', / +/, choice('a', 'an')), seq('isnt', / +/, choice('a', 'an')))), $.instance_equal_not_operator),
        )),
        $.primary_expression
      ))) 
    )),

    cast_expression: $ => prec.left(PREC.invocative, seq(
      field('source', $.expression),
      alias(choice('to', '~>'), $.cast_operator),
      field('target', choice(
        $.identifier,
        $.attribute,
        $.subscript,
        $.call,
        $.type 
      ))
    )),
  
    function_within_for_in_clause: $ => seq(
      field('parameters', $.parameters),
      $.function_operator,
      field('body', $._expression_within_for_in_clause),
    ),
    
    assignment_expression: $ => seq(
      field('left', $._left_hand_side),
      field('operator', choice(
        ':=',
        '.='
      )),
      field('right', $._right_hand_side)
    ),
    
    augmented_assignment_expression: $ => seq(
      field('left', $._left_hand_side),
      field('operator', choice(
        '+=', '-=', '*=', '/=', '%=', '^=', '&='
      )),
      field('right', $._right_hand_side)
    ),
    
    _left_hand_side: $ => choice(
      $.pattern,
      $.pattern_list,
    ),
    
    pattern_list: $ => seq(
      $.pattern,
      choice(
        $.comma,
        seq(repeat1(seq($.comma, $.pattern)), optional($.comma))
      )
    ),
    
    _right_hand_side: $ => choice(
      $.expression,
      $.expression_list,
      $.assignment_expression,
      $.augmented_assignment_expression,
      $.yield_expression
    ),
    
    yield_expression: $ => prec.right(seq(
      'yiel',
      choice(
        seq('from', $.expression),
        optional($._expressions)
      )
    )),
    
    attribute: $ => prec(PREC.invocative, seq(
      field('object', $.primary_expression),
      '\\',
      field('attribute', $.identifier) 
    )),
    
    subscript: $ => prec(PREC.invocative, seq(
      field('value', $.primary_expression),
      '[',
      sep1(field('subscript', choice($.expression, $.slice)), $.comma),
      optional($.comma),
      ']'
    )),
    
    slice: $ => seq(
      optional($.expression),
      'to',
      optional($.expression),
      optional(seq('by', $.expression))
    ),
    
    call: $ => prec(PREC.invocative, seq(
      field('function', $.primary_expression),
      field('arguments', choice(
        $.generator_expression,
        $.argument_list
      ))
    )),
    
    keyword_argument: $ => seq(
      field('name', choice($.identifier, $.keyword_identifier)),
      field('operator', choice(
        ':=',
        '.='
      )),
      field('value', $.expression)
    ),
    
    // Literals
    
    array: $ => coll('[', $._collection_elements, ']', alias('.', $.const_modifier)),
    
    set: $ => coll('<', $._collection_elements, '>', alias('.', $.const_modifier)),
    
    tuple: $ => coll('{', $._collection_elements, '}', alias('.', $.const_modifier)),
    
    lexicon: $ => coll('<[', $._collection_elements, ']>', alias('.', $.const_modifier)),
    
    lexeme: () => token(
      repeat1(/[^\s\]]/)
    ),

    object: $ => {
      const obj_arg = choice($.pair, $.object_spread);
      return seq(
        seq(obj_arg, repeat(prec(PREC.objective, seq($.separator, obj_arg))),
        $.semicolon
      ));
    },
    pair: $ => seq(
      field('key', $.expression),
      field('operator', choice('.', ':')),
      field('value', $.expression)
    ),
    
    array_comprehension: $ => seq(
      '[',
      field('body', $.expression),
      $._comprehension_clauses,
      ']'
    ),
    
    set_comprehension: $ => seq(
      '<',
      field('body', $.expression),
      $._comprehension_clauses,
      '>'
    ),
    
    object_comprehension: $ => seq(
      '{',
      field('body', $.pair),
      $._comprehension_clauses,
      '}'
    ),
    
    generator_expression: $ => seq(
      '(',
      field('body', $.expression),
      $._comprehension_clauses,
      ')'
    ),
    
    _comprehension_clauses: $ => seq(
      $.for_in_clause,
      repeat(choice(
        $.for_in_clause,
        $.if_clause    
      ))
    ),
    
    parenthesized_expression: $ => prec(PREC.parenthesized_expression, seq(
      '(',
      choice($.expression, $.yield_expression),
      ')'
    )),
    
    _collection_elements: $ => seq(
      sep1(choice(
        $.expression,
        $.yield_expression,
        $.array_spread,
        $.parenthesized_array_spread    
      ), $.separator),
      optional($.separator)
    ),
    
    for_in_clause: $ => prec.left(seq(
      optional('async'),
      'for',
      field('left', $._left_hand_side),
      'in',
      field('right', sep1($._expression_within_for_in_clause, $.comma)),
      optional($.comma)
    )),
    
    if_clause: $ => seq('if', $.expression),
    
    conditional_expression: $ => prec.right(PREC.conditional, seq(
      $.expression,
      'if',
      $.expression,
      'else',
      $.expression
    )),
    
    symbol: () => seq(
      '`',
      token.immediate(/[^,;\[\]\(\)\{\}\s]+/)
    ),
    
    string: $ => seq(
      alias($._string_start, '"'),
      repeat(choice($.interpolation, $.escape_sequence, $._not_escape_sequence, $._string_fragment)),
      alias($._string_end, '"')
    ),
    
    interpolation: $ => choice(
      seq('~', $.identifier),
      seq(
        '~{',
        sep1($.expression, $.semicolon),
        '}'
      )
    ),
    
    escape_sequence: () => token(prec(1, seq(
      '\\',
      choice(
        /u[a-fA-F\d]{4}/,
        /U[a-fA-F\d]{8}/,
        /x[a-fA-F\d]{2}/,
        /\d{3}/,
        /\r?\n/,
        /['"abfrntv\\]/,
      ) 
    ))),
    
    _not_escape_sequence: () => '\\',
    
    integer: () => token(choice(
      seq(
        choice('0x', '0X'),
        repeat1(/_?[A-Fa-f0-9]+/),
        optional(/[Ll]/)
      ),
      seq(
        choice('0o', '0O'),
        repeat1(/_?[0-7]+/),
        optional(/[Ll]/)
      ),
      seq(
        choice('0b', '0B'),
        repeat1(/_?[0-1]+/),
        optional(/[Ll]/)
      ),
      seq(
        repeat1(/[0-9]+_?/),
        choice(
          optional(/[Ll]/), // long numbers
          optional(/[jJ]/) // complex numbers
        )
      )
    )),
    
    float: () => {
      const digits = repeat1(/[0-9]+_?/);
      const exponent = seq(/[eE][\+\-]?/, digits);
      
      return token(seq(
        choice(
          seq(digits, '.', optional(digits), optional(exponent)),
          seq(optional(digits), '.', digits, optional(exponent)),
          seq(digits, exponent)
        )
      ))
    },
    
    identifier: () => /[_\-\p{XID_Start}][_\-\p{XID_Continue}]*/,
    
    keyword_identifier: $ => prec(-3, alias(
      choice(
        'print',
        'exec',
        'async',
        'await',
      ),
      $.identifier
    )),
    
    type: () => choice(
      'Integer',
      'Float',
      'String',
      'Symbol',
      'Object',
      'Function',
      'Array',
      'Set',
      'WeakSet',
      'Lexicon',
      'Tuple',
      'Boolean',
      'Map',
      'WeakMap',
      'RegExp',
      'Error',
    ),
    true: () => /true|on/,
    false: () => /false|off/,
    none: () => /undefined|none/,
    empty: () => /null|empty/,
    
    await: $ => prec(PREC.unary, seq(
      'await',
      $.expression 
    )),
    
    comment: () => token(choice(
      seq('#', /.*/),
      seq('#[', /.*/, ']#')    
    )),
    terminator: $ => prec.right(choice(
      $.semicolon,
      $.newline,
      seq($.semicolon, $.newline)
    )),
    semicolon: () => ';',
    separator: $ => prec.right(choice(
      $.comma,
      $.newline,
      seq($.comma, $.newline)
    )),
    ind_separator: $ => prec.right(
      seq(optional($.comma), $.newline, $.dedent)
    ),
    comma: () => ',',
  },
})
     
// a repeating list separated by some separator
function sep1(rule, separator) {
  return seq(
    rule,
    repeat(seq(separator, rule))
  );
}

function coll(open, elements, close, modifier) {
  var mod_open;
  if (modifier) {
    mod_open = choice(
      seq(optional(modifier), token.immediate(open)),
      open
    )
  } else {
    mod_open = open;
  }
  return seq(
      mod_open,
      optional(elements),
      close
  );
}

// function splat(rule) { return prec.right(seq( optional(rule), '...')) }