#!/bin/sh

SPINNER=""
echo "Using Fira Code spinner..."

echo -n ' '
while true; do
  sleep 0.05
  printf "\r${SPINNER:i++%${#SPINNER}:1}"
done &
SPINNER_PID=$!
trap 'kill "$SPINNER_PID"' EXIT
./node_modules/.bin/tree-sitter parse $1 > OUTPUT
kill "$SPINNER_PID"
printf "\r"
echo "$OUTPUT"